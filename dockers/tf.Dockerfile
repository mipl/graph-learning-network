FROM tensorflow/tensorflow:1.7.0-gpu-py3

MAINTAINER mipl

RUN apt-get -y update -qq && \
    apt-get -y install \
      python3-tk

RUN pip3 install --no-cache-dir --upgrade pip &&\
    pip3 install --no-cache-dir \
      community \
      numpy \
      pyemd \
      scikit-image \
      scipy \
      sklearn \
      tqdm 

# Exposing jupyter and tensorboard
EXPOSE 8888 6006

# Copy the source
COPY GLN.py utils.py /home/
COPY datasets/*.py /home/datasets/
COPY eval/ /home/eval/

# Change working dirs
WORKDIR /home