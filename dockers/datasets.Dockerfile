FROM python:3.7
MAINTAINER mipl

RUN pip3 install --no-cache-dir --upgrade pip &&\
    pip3 install --no-cache-dir \
      community \
      matplotlib \
      networkx \
      scikit-learn \
      tqdm

COPY ./datasets/*.py /home/

# Change working dirs
WORKDIR /home
ENTRYPOINT ["/bin/bash"]