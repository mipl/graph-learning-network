import graph_communty_dataset
import graph_surface_dataset
import geometric_shape_dataset
import argparse
import numpy as np
import os

parser = argparse.ArgumentParser(description='Generate synthetic dataset')
parser.add_argument('--type-dataset', default='geo_img',
                    help='type of dataset to generate')
parser.add_argument('--create-data', default=True, type=bool,
                    help='save dataset as .csv file')
parser.add_argument('--create-image', default=True, type=bool,
                    help='save image of the dataset as .png')
parser.add_argument('--seed', default=123, type=int,
                    help='seed for generate the dataset')
parser.add_argument('--output', default='out/',
                    help='target output directory to generate the images')
args = parser.parse_args()

#python3 General_data.py --type-dataset surf

print("-----> ", args.type_dataset)
print("-----> ", args.seed)

# Create needed structure on output
dirs = ["features", "graphs", "img"]
[os.makedirs(f"{os.path.join(args.output, d)}", exist_ok=True) for d in dirs]

if args.type_dataset == 'geo_img':
  num_samples = 3000
  num_nodes = 10*10 #square
  dim_h = int(np.sqrt(num_nodes))
  dim_w = int(np.sqrt(num_nodes))
  type_neighborhood = "D4"
  geometric_shape_dataset.generate_save_dataset(num_samples=num_samples, dim_h=dim_h, dim_w=dim_w, \
      type_neig=type_neighborhood, create_data=args.create_data, create_image=args.create_image, seed=args.seed, save_dir=args.output)

elif args.type_dataset == 'surface':
  num_surfaces = 1000
  num_points = 10*10 #square
  #types=['elliptic_paraboloid','saddle','torus','ellipsoid','elliptic_hyperboloid','another', 'all']
  types = 'all'
  num_perm = 3

  graph_surface_dataset.generate_save_dataset(num_surfaces=num_surfaces, num_points=num_points, type_surf=types, \
      num_perm=num_perm, create_data=args.create_data, create_image=args.create_image, seed=args.seed, save_dir=args.output)

elif args.type_dataset == 'community':
  #types=['caveman_2','caveman_4']
  types = 'caveman_4'
  num_perm = 3

  graph_communty_dataset.generate_save_dataset(type_comm=types, num_perm=num_perm, \
      create_data=args.create_data, create_image=args.create_image, seed=args.seed, save_dir=args.output)
