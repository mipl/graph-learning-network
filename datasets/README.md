
# Synthetic Graph Datasets

## 3D Surface Dataset
It is a collection of the following 3D surfaces functions: **torus**, **elliptic paraboloid**, **saddle**, **ellipsoid**, **elliptic hyperboloid**, **another**. Where we use different geometric transformations(scale, translate, rotate, reflex, shear) to give variability to the samples. It is possible to change the number of point by surface, the number or type of functions.<br>The code is shown in `graph_surface_dataset.py`. 

<table>
  <tr>
    <th align="center">Torus</th>
    <th align="center">Elliptic<br>Hyperboloid</th>
    <th align="center">Elliptic<br>Paraboloid</th>
    <th align="center">Ellipsoid</th>
    <th align="center">Saddle</th>
  </tr>
  <tr>
    <td><img src="imgs/surf_1_100.png" alt="non-trivial image" width="100%" align="center"></td>
    <td><img src="imgs/surf_2_100.png" alt="non-trivial image" width="100%" align="center"></td>
    <td><img src="imgs/surf_3_100.png" alt="non-trivial image" width="100%" align="center"></td>
    <td><img src="imgs/surf_4_100.png" alt="non-trivial image" width="100%" align="center"></td>
    <td><img src="imgs/surf_5_100.png" alt="non-trivial image" width="100%" align="center"></td>
  </tr>
  <tr>
    <td><img src="imgs/surf_6_100.png" alt="non-trivial image" width="100%" align="center"></td>
    <td><img src="imgs/surf_7_100.png" alt="non-trivial image" width="100%" align="center"></td>
    <td><img src="imgs/surf_8_100.png" alt="non-trivial image" width="100%" align="center"></td>
    <td><img src="imgs/surf_9_100.png" alt="non-trivial image" width="100%" align="center"></td>
    <td><img src="imgs/surf_10_100.png" alt="non-trivial image" width="100%" align="center"></td>
  </tr>
  <tr>
    <th align="center">Torus</th>
    <th align="center">Elliptic<br>Hyperboloid</th>
    <th align="center">Elliptic<br>Paraboloid</th>
    <th align="center">Ellipsoid</th>
    <th align="center">Saddle</th>
  </tr>
  <tr>
    <td><img src="imgs/surf_1_400.png" alt="non-trivial image" width="100%" align="center"></td>
    <td><img src="imgs/surf_2_400.png" alt="non-trivial image" width="100%" align="center"></td>
    <td><img src="imgs/surf_3_400.png" alt="non-trivial image" width="100%" align="center"></td>
    <td><img src="imgs/surf_4_400.png" alt="non-trivial image" width="100%" align="center"></td>
    <td><img src="imgs/surf_5_400.png" alt="non-trivial image" width="100%" align="center"></td>
  </tr>
  <tr>
    <td><img src="imgs/surf_6_400.png" alt="non-trivial image" width="100%" align="center"></td>
    <td><img src="imgs/surf_7_400.png" alt="non-trivial image" width="100%" align="center"></td>
    <td><img src="imgs/surf_8_400.png" alt="non-trivial image" width="100%" align="center"></td>
    <td><img src="imgs/surf_9_400.png" alt="non-trivial image" width="100%" align="center"></td>
    <td><img src="imgs/surf_10_400.png" alt="non-trivial image" width="100%" align="center"></td>
  </tr>
</table>


### Generate Surface Dataset

This dataset is generated through
  
  ```python
  graph_surface_dataset.generate_save_dataset(num_surfaces, num_points, type_surf, num_perm, create_data, create_image, seed)
  ```
  
  - `num_surfaces` is the number of graphs generated.
  - `num_points` is the number of nodes generated for each graph.
  - `type_surf` is the type the graphs generated. You can use the following types, `['elliptic_paraboloid','saddle','torus','ellipsoid','elliptic_hyperboloid','another', 'all']`
  - `num_perm` is the number of permutation for each graph created.
  - `create_data` is `True` if you want to save data of the graphs as `*.csv`.
  - `create_image` is `True` if you want to save image of the graphs as `*.png`.
  - `seed` is the seed that we use to generate the data.
  
Additionally, we provide the function `test_batch_gen()`, which works as an example to feed your model. Run the following command `python3 graph_surface_dataset.py.py`.



## Community Dataset

This dataset presents two sets with $`C=2`$ and $`C=4`$ communities with $`40`$ and $`80`$ vertices each, respectively, created with the caveman algorithm, [Watts1999](https://www.cc.gatech.edu/~mihail/D.8802readings/watts-swp.pdf), where each community has $`20`$ people.  Besides, Community $`C=4`$ and $`C=2`$ have $`500`$ and $`300`$ samples respectively.<br>The code is show in `graph_communty_dataset.py`. 

<table>
  <tr>
    <th colspan="3">Community C=2</th>
  </tr>
  <tr>
    <th><img src="imgs/comm_20.png" alt="non-trivial image" width="100%" align="center"></th>
    <th><img src="imgs/comm_24.png" alt="non-trivial image" width="100%" align="center"></th>
    <th><img src="imgs/comm_28.png" alt="non-trivial image" width="100%" align="center"></th>
  </tr>
  <tr>
    <th colspan="3">Community C=4</th>
  </tr>
  <tr>
    <td><img src="imgs/comm_00.png" alt="non-trivial image" width="100%" align="center"></td>
    <td><img src="imgs/comm_04.png" alt="non-trivial image" width="100%" align="center"></td>
    <td><img src="imgs/comm_08.png" alt="non-trivial image" width="100%" align="center"></td>
  </tr>
</table>



### Generate Community Dataset

This dataset is generated through

    ```python
    graph_communty_dataset.generate_save_dataset(type_comm, num_perm, create_data, create_image, seed)
    ```
    
  - `type_comm` is the type the graphs generated. You can use the following types, `['caveman_2','caveman_4']`
  - `num_perm` is the number of permutation for each graph created.
  - `create_data` is `True` if you want to save data of the graphs as `*.csv`.
  - `create_image` is `True` if you want to save image of the graphs as `*.png`.
  - `seed` is the seed that we use to generate the data.
    
Additionally we provide the function `test_batch_gen()`, which works as an example to feed your model. Run the following command, `python3 graph_communty_dataset.py`.



## Geometric Figures

This dataset contain three types of geometric figures (square, rectangle and line). Where we use different geometric transformations(scale, translate, rotate). It is possible to change the image size $`(H,W)`$ and choose the type of geometric figure that contain each synthetic image.<br> The code is show in `geometric_shape_dataset`.

<table>
  <tr>
    <th colspan="5">Neighborhood D4</th>
  </tr>
  <tr>
    <th><img src="imgs/img_22_10_D4.png" alt="non-trivial image" width="100%" align="center"></th>
    <th><img src="imgs/img_29_10_D4.png" alt="non-trivial image" width="100%" align="center"></th>
    <th><img src="imgs/img_34_10_D4.png" alt="non-trivial image" width="100%" align="center"></th>
    <th><img src="imgs/img_40_10_D4.png" alt="non-trivial image" width="100%" align="center"></th>
    <th><img src="imgs/img_52_10_D4.png" alt="non-trivial image" width="100%" align="center"></th>
  </tr>
  <tr>
    <th><img src="imgs/img_10_20_D4.png" alt="non-trivial image" width="100%" align="center"></th>
    <th><img src="imgs/img_1_20_D4.png" alt="non-trivial image" width="100%" align="center"></th>
    <th><img src="imgs/img_13_20_D4.png" alt="non-trivial image" width="100%" align="center"></th>
    <th><img src="imgs/img_24_20_D4.png" alt="non-trivial image" width="100%" align="center"></th>
    <th><img src="imgs/img_26_20_D4.png" alt="non-trivial image" width="100%" align="center"></th>
  </tr>
  <tr>
    <th colspan="5">Neighborhood D8</th>
  </tr>
  <tr>
    <th><img src="imgs/img_10_10_D8.png" alt="non-trivial image" width="100%" align="center"></th>
    <th><img src="imgs/img_12_10_D8.png" alt="non-trivial image" width="100%" align="center"></th>
    <th><img src="imgs/img_20_10_D8.png" alt="non-trivial image" width="100%" align="center"></th>
    <th><img src="imgs/img_6_10_D8.png" alt="non-trivial image" width="100%" align="center"></th>
    <th><img src="imgs/img_8_10_D8.png" alt="non-trivial image" width="100%" align="center"></th>
  </tr>
  <tr>
    <th><img src="imgs/img_0_20_D8.png" alt="non-trivial image" width="100%" align="center"></th>
    <th><img src="imgs/img_16_20_D8.png" alt="non-trivial image" width="100%" align="center"></th>
    <th><img src="imgs/img_18_20_D8.png" alt="non-trivial image" width="100%" align="center"></th>
    <th><img src="imgs/img_4_20_D8.png" alt="non-trivial image" width="100%" align="center"></th>
    <th><img src="imgs/img_8_20_D8.png" alt="non-trivial image" width="100%" align="center"></th>
  </tr>
</table>


### Generate Surface Dataset

This dataset is generated through
  ```python
  geometric_shape_dataset.generate_save_dataset(num_samples, dim_h, dim_w, type_neig, create_data, create_image, seed)
  ```

  - `num_samples` is the number of graphs generated.
  - `dim_h` is the height of the images generated.
  - `dim_w` is the width of the images generated.
    Note, the dim must be equals `dim_h = dim_w`
  - `type_neig` is the type the graphs connections. You can use the following types, `['D4','D8']`
  - `create_data` is `True` if you want to save data of the graphs as `*.csv`.
  - `create_image` is `True` if you want to save image of the graphs as `*.png`.
  - `seed` is the seed that we use to generate the data.

Additionally we provide the function `test_batch_gen()`, which works as an example to feed your model. Run the following command, `python3 geometric_shape_dataset.py.py`.

## Generate Datasets


### Run the code

You need to have the following directory structure to run each script

  ```
  -- out
      |-- features
      |-- graphs
      |-- img
  ```

  - `features` directory saves the `*.csv` files of the node's features.
  - `graphs` directory saves the `*.csv` files of the graphs (nodes and edges).
  - `img` directory saves the  image `*.png` of the graphs.

To help with it, we provide a general generation script that creates the structure as needed, and then calls the corresponding generation script.

  ```bash
     python3 general_data.py --type-dataset surface --seed 123
  ```

The possible types are `surface`, `community`, and `geo_img`.

### Docker

We are also making available a docker that encapsulates all the generation.  You can run the docker and mount a local folder to obtain the output.  For instance

  ```bash
     docker run --rm --volume `pwd`:/home/out mipl/gln:datasets -c python general_data.py --type-dataset surface --seed 123
  ```

In this case, we are assuming the default directory `out` for the output (and the docker is built to use `/home` as the working directory).  You can use any other combination, for example, run using `surf` as your output directory

  ```bash
     docker run --rm --volume `pwd`:/home/surf mipl/gln:datasets -c python general_data.py --type-dataset surface --seed 123  --output surf
  ```

Note the changes on both the mount directory and the output option of the script.